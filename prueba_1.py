"""
    Punto a
"""
for x in range(0, 101):
    print(x, 'buzz' if x % 2 == 0 else '', 'bazz' if x % 5 == 0 else '')
"""
    Punto b
"""
pokemon_names = "audino bagon baltoy banette bidoof braviary bronzor carracosta charmeleon cresselia croagunk darmanitan deino emboar emolga exeggcute gabite girafarig gulpin haxorus heatmor heatran ivysaur jellicent jumpluff kangaskhan kricket landorus ledyba loudred lumineon lunatone machamp magnezone mamoswine nosepass petilil pidgeotto pikachu pinsir poliwrath poochyena porygon2 porygonz registeel relicanth remoraid rufflet sableye scolipede scrafty seaking sealeo silcoon simisear snivy snorlax spoink starly tirtouga trapinch treecko tyrogue vigoroth vulpix wailord wartortle whismur wingull yamask"
pokemon_list = pokemon_names.split()

pokemon_json = {}
for pokemon in pokemon_list:
    if pokemon[0] in pokemon_json:
        pokemon_json[pokemon[0]].append(pokemon)
    else:
        pokemon_json[pokemon[0]] = [pokemon]

def funcion(lista):
    ultima_letra = lista[-1][-1]
    opciones = pokemon_json[ultima_letra] if ultima_letra in pokemon_json else []

    for item in lista:
        if item in opciones:
            opciones.remove(item)

    if not opciones:
        return lista
    else:
        return max((funcion(lista + [i]) for i in opciones), key=len)

print(max((funcion([pokemon]) for pokemon in pokemon_list), key=len))

