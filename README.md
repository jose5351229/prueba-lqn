# SW API GraphQL

## Requirements
* [Python](https://www.python.org/) (realizado en python 3.8)
* [Virtualenv](https://virtualenv.pypa.io/en/latest/)

## Setup

Clone the project
```
git clone https://github.com/gustav0/swapi.git
```

Create Virtual env
```
virtualenv <name_env> -p python3.8
source name_env/bin/activate
```

Install dependencies
```
pip install -r requirements.txt
```

Run migrations and load fixtures
```
find . -path "*/migrations/*.py" -not -name "__init__.py" -delete
find . -path "*/migrations/*.pyc" -delete
python manage.py makemigrations
python manage.py migrate
python manage.py load_fixtures
```
Now the project is all set.

### Running the server
```
python manage.py runserver
```

### Runing the tests
```
python manage.py test
```

## Extras
Actualice la coleccion de postman con el nuevo request de characters
